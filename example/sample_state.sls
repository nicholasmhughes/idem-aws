{%  for i in range(1) %}
idem_aws_demo_user_{{i}}:
  aws.iam.user.present
{%  endfor -%}

my_role:
  aws.iam.role.present:
    - assume_role_policy_document: "{}"


Vpcs[?Tags[?Key=='idem_demo_key']|[?Value=='my_vpc']].VpcId:
  aws.ec2.vpc.present:
    - cidr_block: 10.0.0.0/24
    - tag_specifications:
      - ResourceType: dhcp
        Tags:
        - Key: idem_demo_key
          Value: my_vpc

Subnets[?Tags[?Key=='idem_test_key']|[?Value=='my_subnet']].SubnetId:
  aws.ec2.subnet.present:
    - vpc: Vpcs[?Tags[?Key=='idem_demo_key']|[?Value=='my_vpc']].VpcId|[0]
    - cidr_block: 10.0.0.1/24
    - tag_specifications:
      - ResourceType:
        Tags:
        - Key: idem_demo_keys
          Value: my_subnet

Reservations[].Instances[]|[?Tags[?Key=='idem_demo_key']|[?Value=='my_instance']].InstanceId:
  aws.ec2.instance.present:
    - subnet: Subnets[?Tags[?Key=='idem_test_key']|[?Value=='my_subnet']].SubnetId|[-1]
    - tags:
      idem_demo_key: instance_name
