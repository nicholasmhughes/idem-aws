import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_security_group(hub, ctx, aws_ec2_vpc):
    # create security_group
    security_group_id = "idem-test-security-group-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": security_group_id},
    ]
    security_group_rules = [
        {
            "CidrIpv4": "0.0.0.0/0",
            "FromPort": 22,
            "IpProtocol": "tcp",
            "IsEgress": False,
            "Tags": [],
            "ToPort": 22,
        },
    ]
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=ctx,
        name=security_group_id,
        description=f"Created for Idem integration test.",
        sg_rules=security_group_rules,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert security_group_id == ret["name"]
    assert tags == ret["new_state"]["Tags"]
    ip_permission_ingress = ret["new_state"]["IpPermissions"][0]
    assert security_group_rules[0].get("FromPort") == ip_permission_ingress.get(
        "FromPort"
    )
    assert security_group_rules[0].get("IpProtocol") == ip_permission_ingress.get(
        "IpProtocol"
    )
    ip_permission_egress = ret["new_state"]["IpPermissionsEgress"][0]
    # Default egress rule
    assert "-1" == ip_permission_egress.get("IpProtocol")
    assert "0.0.0.0/0" == ip_permission_egress.get("IpRanges")[0].get("CidrIp")
    created_security_group_id = ret["new_state"]["GroupId"]

    # verify that created security group is present
    describe_ret = await hub.states.aws.ec2.security_group.describe(ctx)
    assert created_security_group_id in describe_ret

    # describe rules to get existing rules
    describe_rules_ret = await hub.exec.boto3.client.ec2.describe_security_group_rules(
        ctx,
        Filters=[{"Name": "group-id", "Values": [created_security_group_id]}],
    )

    if describe_rules_ret["result"]:
        existing_sg_rules = describe_rules_ret["ret"].get("SecurityGroupRules")
        existing_sg_rules[0].update({"FromPort": -1})
        existing_sg_rules[0].update({"ToPort": -1})
        existing_sg_rules[0].update({"IpProtocol": "-1"})
        del existing_sg_rules[1]
        existing_sg_rules.append(
            {
                "CidrIpv4": "0.0.0.0/0",
                "FromPort": 80,
                "IpProtocol": "tcp",
                "IsEgress": False,
                "ToPort": 80,
            }
        )
        ret = await hub.states.aws.ec2.security_group.present(
            ctx=ctx,
            name=created_security_group_id,
            description=f"Created for Idem integration test.",
            sg_rules=existing_sg_rules,
            vpc_id=aws_ec2_vpc.get("VpcId"),
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert created_security_group_id == ret["name"]
        # Describe Security Group
        describe_ret = await hub.states.aws.ec2.security_group.describe(ctx)
        assert created_security_group_id in describe_ret
        # Verify that describe output format is correct
        assert "aws.ec2.security_group.present" in describe_ret.get(
            created_security_group_id
        )
        described_resource = describe_ret.get(created_security_group_id).get(
            "aws.ec2.security_group.present"
        )
        described_resource_map = dict(ChainMap(*described_resource))

        assert "sg_rules" in described_resource_map

        new_sg_rules = described_resource_map.get("sg_rules")
        assert 2 == len(new_sg_rules)
        for rule in new_sg_rules:
            if rule.get("FromPort") == -1:
                assert "-1" == rule.get("IpProtocol")
            else:
                assert 80 == rule.get("FromPort")
                assert 80 == rule.get("ToPort")

        assert tags == described_resource_map.get("tags")

    # fail if the test is running in real aws account and unable to describe security group rules
    elif ctx["acct"].get("aws_access_key_id") != "localstack":
        pytest.fail("Unable to get security group rules")

    # Delete instance
    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=created_security_group_id
    )
    assert f"Deleted '{created_security_group_id}'" in ret["comment"]

    # Deleting the same instance again should state the same in comment
    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=created_security_group_id
    )
    assert (
        f"'{created_security_group_id}' is already in deleted state." in ret["comment"]
    )
