import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_route_table(
    hub, ctx, aws_ec2_vpc, aws_ec2_subnet, aws_ec2_transit_gateway
):
    # Create route table
    route_table_temp_name = "idem-test-route-table" + str(uuid.uuid4())
    vpc_id = aws_ec2_vpc.get("VpcId")
    tags = [{"Key": "Name", "Value": route_table_temp_name}]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        vpc_id=vpc_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert vpc_id == resource.get("VpcId")
    assert tags == resource.get("Tags")
    resource_id = resource.get("RouteTableId")
    new_routes = resource.get("Routes")
    new_routes.append(
        {
            "DestinationCidrBlock": "172.31.1.0/16",
            "TransitGatewayId": aws_ec2_transit_gateway.get("TransitGatewayId"),
        }
    )

    # remove main route table as we cannot modify
    new_associations = [
        new_association
        for new_association in resource.get("Associations")
        if new_association.get("Main") is not True
    ]

    new_associations.append({"SubnetId": aws_ec2_subnet.get("SubnetId")})
    # Test updating tags, routes and associations
    new_tags = [
        {"Key": "new-name", "Value": resource_id},
    ]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        routes=new_routes,
        associations=new_associations,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("Tags")
    new_route_added = resource.get("Routes")[1]
    new_association_added = [
        new_association
        for new_association in resource.get("Associations")
        if new_association.get("Main") is not True
    ][0]
    assert (
        new_route_added["DestinationCidrBlock"] == new_routes[1]["DestinationCidrBlock"]
    )
    assert new_route_added["TransitGatewayId"] == new_routes[1]["TransitGatewayId"]
    assert new_association_added["SubnetId"] == new_associations[0]["SubnetId"]
    assert new_association_added["RouteTableAssociationId"]
    assert new_association_added["RouteTableId"] == resource_id

    # Removing routes with associations and tags unchanged
    new_routes = [new_routes[0]]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=new_routes,
        associations=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("Routes")

    # Removing all associations with routes and tags unchanged
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=None,
        associations=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    new_associations = [
        new_association
        for new_association in resource.get("Associations")
        if new_association.get("Main") is not True
    ]
    assert new_associations == []
    assert new_routes == resource.get("Routes")
    # Describe route_table
    describe_ret = await hub.states.aws.ec2.route_table.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.ec2.route_table.present"], "tags", new_tags
    )

    # Delete route_table
    ret = await hub.states.aws.ec2.route_table.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.ec2.route_table.absent(ctx, name=resource_id)
    assert ret["comment"] == f"'{resource_id}' already absent"
