import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_standard_elastic_ip(hub, ctx):
    # Create elastic ip for standard domain
    elastic_ip_temp_name = "idem-test-elastic-ip-" + str(uuid.uuid4())
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=elastic_ip_temp_name, domain="standard"
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("PublicIp")

    # Describe elastic_ip
    describe_ret = await hub.states.aws.ec2.elastic_ip.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.elastic_ip.present"
    )

    # Delete elastic_ip
    ret = await hub.states.aws.ec2.elastic_ip.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.elastic_ip.absent(ctx, name=resource_id)
    assert ret["comment"] == f"'{resource_id}' already absent"


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_elastic_ip_with_vpc(hub, ctx):
    # Create elastic ip for vpc domain
    elastic_ip_temp_name = "idem-test-elastic-ip-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": elastic_ip_temp_name}]
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=elastic_ip_temp_name, domain="vpc", tags=tags
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")

    assert tags == resource.get("Tags")
    resource_id = resource.get("PublicIp")

    # Update tags
    new_tags = [{"Key": "Name", "Value": "Updated"}]
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=resource_id, domain="vpc", tags=new_tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret["new_state"]
    assert new_tags == new_resource.get("Tags")
    # test for removal of old tags having same name and different value
    old_resource = ret["old_state"]
    assert tags == old_resource.get("Tags")

    # Describe elastic_ip
    describe_ret = await hub.states.aws.ec2.elastic_ip.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.elastic_ip.present"
    )
    resource = describe_ret.get(resource_id).get("aws.ec2.elastic_ip.present")
    describe_tags = None
    for item in resource:
        if "tags" in item:
            describe_tags = item.get("tags")

    assert describe_tags == new_tags

    # Delete elastic_ip
    ret = await hub.states.aws.ec2.elastic_ip.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.elastic_ip.absent(ctx, name=resource_id)
    assert ret["comment"] == f"'{resource_id}' already absent"
