import json
import uuid

import pytest


@pytest.mark.asyncio
async def test_role(hub, ctx):
    # Create IAM role
    role_temp_name = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Action": "s3:ListBucket","Resource": "arn:aws:s3:::example_bucket"}}'
    description = "Idem IAM role test description"
    max_session_duration = 3700
    tags = [{"Key": "Name", "Value": role_temp_name}]
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert json.dumps(
        json.loads(assume_role_policy_document), separators=(", ", ": ")
    ) == resource.get("assume_role_policy_document")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")

    # Verify present w/o changes does not trigger an update
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "already exists" in ret["comment"], "Should not update existing role"

    # Test updating description, max_session_duration and adding tags
    description = "Idem IAM role test description updated"
    max_session_duration = 3800
    tags.append(
        {
            "Key": f"idem-test-iam-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-iam-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        max_session_duration=max_session_duration,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert description == resource.get("description")
    assert max_session_duration == resource.get("max_session_duration")
    assert tags == resource.get("tags")

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_temp_name,
        resource_id=resource_id,
        assume_role_policy_document=assume_role_policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret.get("old_state")
    resource = ret.get("new_state")
    assert old_resource.get("description") == resource.get("description")
    assert old_resource.get("max_session_duration") == resource.get(
        "max_session_duration"
    )
    assert tags == resource.get("tags")

    # Describe IAM role
    describe_ret = await hub.states.aws.iam.role.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.iam.role.present"], "tags", tags
    )

    # Delete IAM role - require name for deletion
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM role again
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=role_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
