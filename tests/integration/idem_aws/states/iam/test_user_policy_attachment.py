import uuid

import pytest


@pytest.mark.asyncio
async def test_user_policy_attachment(hub, ctx, aws_iam_user):
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
    user_name = aws_iam_user.get("UserName")

    attach_user_policy_name = user_name + str(uuid.uuid4())
    # Attach an IAM managed policy to an IAM user
    ret = await hub.states.aws.iam.user_policy_attachment.present(
        ctx,
        name=attach_user_policy_name,
        user_name=user_name,
        policy_arn=policy_arn,
    )
    resource = ret.get("new_state")
    assert policy_arn == resource.get("PolicyArn")
    assert f"Attached '{attach_user_policy_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    # Describe attached user policies
    describe_ret = await hub.states.aws.iam.user_policy_attachment.describe(ctx)
    assert f"{user_name}-{policy_arn}" in describe_ret

    # Detach an IAM managed policy
    ret = await hub.states.aws.iam.user_policy_attachment.absent(
        ctx,
        name=attach_user_policy_name,
        user_name=user_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert f"Detached '{attach_user_policy_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Describe attached policies again and verify that detached policy is not listed
    describe_ret = await hub.states.aws.iam.user_policy_attachment.describe(ctx)
    assert f"{user_name}-{policy_arn}" not in describe_ret

    # Detach again an IAM managed policy for an IAM user
    ret = await hub.states.aws.iam.user_policy_attachment.absent(
        ctx,
        name=attach_user_policy_name,
        user_name=user_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert f"'{attach_user_policy_name}' already absent" in ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
