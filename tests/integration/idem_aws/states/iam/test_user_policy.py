import uuid

import pytest


@pytest.mark.asyncio
async def test_user_policy(hub, ctx, aws_iam_user):
    policy_name = "idem-test-user-policy-" + str(uuid.uuid4())
    user_name = aws_iam_user.get("UserName")
    policy_document = '{"Version": "2012-10-17","Statement":  {"Effect": "Allow", "Action": ["ec2:Describe*"], "Resource": "*"}}'
    # Create IAM user policy
    ret = await hub.states.aws.iam.user_policy.present(
        ctx,
        name=policy_name,
        user_name=user_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"]["new"]
    assert f"Created '{policy_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert policy_name == resource.get("PolicyName")
    policy_name = resource.get("PolicyName")

    # Describe IAM user policy
    describe_ret = await hub.states.aws.iam.user_policy.describe(ctx)
    assert policy_name in describe_ret

    # Delete IAM user policy
    ret = await hub.states.aws.iam.user_policy.absent(
        ctx, name=policy_name, user_name=user_name
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")
    assert f"Deleted '{policy_name}'" in ret["comment"]

    # Describe IAM user policy again to verify that deleted policy is not present
    describe_ret = await hub.states.aws.iam.user_policy.describe(ctx)
    assert policy_name not in describe_ret
