import uuid

import pytest


@pytest.mark.asyncio
async def test_role_policy_attachment(hub, ctx, aws_iam_role):
    # Attach an IAM managed policy to an IAM role
    attach_role_policy_temp_name = "idem-test-role-policy-attachment-" + str(
        uuid.uuid4()
    )
    role_name = aws_iam_role.get("name")
    policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_arn == resource.get("policy_arn")

    # Create again
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists" in ret["comment"], ret["comment"]
    resource = ret.get("new_state")
    assert policy_arn == resource.get("policy_arn")

    # Describe attached role policies
    describe_ret = await hub.states.aws.iam.role_policy_attachment.describe(ctx)
    assert f"{role_name}-{policy_arn}" in describe_ret

    # Detach an IAM managed policy to an IAM role
    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Detach again an IAM managed policy to an IAM role
    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_role_policy_temp_name,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
