import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_hosted_zone(hub, ctx):
    # Create hosted_zone
    hosted_zone_name = "idem-test-hosted-zone-" + str(uuid.uuid4())
    caller_reference = "caller_reference"

    hosted_zone_comment = "new hosted zone"
    ret = await hub.states.aws.route53.hosted_zone.present(
        ctx,
        name="hosted_zone/" + str(uuid.uuid4()),
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        config={"Comment": hosted_zone_comment},
        tags=[{"Key": "key1", "Value": "value1"}],
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert ret["new_state"]["tags"]
    assert "key1" == ret["new_state"]["tags"][0].get("Key")
    assert "value1" == ret["new_state"]["tags"][0].get("Value")

    if hub.tool.utils.is_running_localstack(ctx):
        assert hosted_zone_name + "." == ret["new_state"]["hosted_zone_name"]
    else:
        assert hosted_zone_name == ret["new_state"]["hosted_zone_name"]

    resource = ret.get("new_state")
    created_hosted_zone_id = resource.get("resource_id")

    # verify that created hosted_zone is present
    describe_ret = await hub.states.aws.route53.hosted_zone.describe(ctx)
    assert created_hosted_zone_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.route53.hosted_zone.present" in describe_ret.get(created_hosted_zone_id)
    described_resource = describe_ret.get(created_hosted_zone_id).get(
        "aws.route53.hosted_zone.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert "config" in described_resource_map
    assert not described_resource_map["config"]["PrivateZone"]
    assert hosted_zone_comment == described_resource_map["config"]["Comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert hosted_zone_name + "." == described_resource_map.get("hosted_zone_name")
    else:
        assert hosted_zone_name == described_resource_map.get("hosted_zone_name")

    # update tags
    add_tags = [
        {"Key": "new-name", "Value": "test"},
    ]

    # Add tags
    ret_update_tag = await hub.states.aws.route53.hosted_zone.present(
        ctx,
        name=created_hosted_zone_id,
        resource_id=created_hosted_zone_id,
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        tags=add_tags,
    )

    assert ret_update_tag["result"], ret_update_tag["comment"]
    assert ret_update_tag["old_state"] and ret_update_tag["new_state"]
    assert ret_update_tag["old_state"]["tags"] and ret_update_tag["new_state"]["tags"]
    if not hub.tool.utils.is_running_localstack(ctx):
        assert "new-name" == ret_update_tag["new_state"]["tags"][0].get("Key")
        assert "test" == ret_update_tag["new_state"]["tags"][0].get("Value")

    # Update tags

    new_tags = [
        {"Key": "new-name1", "Value": "test1"},
    ]
    ret_update_tag = await hub.states.aws.route53.hosted_zone.present(
        ctx,
        name=created_hosted_zone_id,
        resource_id=created_hosted_zone_id,
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        tags=new_tags,
    )

    assert ret_update_tag["result"], ret_update_tag["comment"]
    assert ret_update_tag["old_state"] and ret_update_tag["new_state"]
    assert ret_update_tag["old_state"]["tags"] and ret_update_tag["new_state"]["tags"]
    if not hub.tool.utils.is_running_localstack(ctx):
        assert 1 == len(ret_update_tag["old_state"]["tags"])
        assert 1 == len(ret_update_tag["new_state"]["tags"])
        assert "new-name1" == ret_update_tag["new_state"]["tags"][0].get("Key")
        assert "test1" == ret_update_tag["new_state"]["tags"][0].get("Value")

    # Delete instance
    ret = await hub.states.aws.route53.hosted_zone.absent(
        ctx,
        name=created_hosted_zone_id,
        resource_id=created_hosted_zone_id,
    )

    assert ret["result"] and ret["comment"]
    assert f"Deleted '{created_hosted_zone_id}'" in ret["comment"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.route53.hosted_zone.absent(
        ctx,
        name=created_hosted_zone_id,
        resource_id=created_hosted_zone_id,
    )

    assert ret["result"] and ret["comment"]
    assert f"'{created_hosted_zone_id}' is already absent." in ret["comment"]
